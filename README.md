# tileMapForWatchFaces

Python script to create tilemaps for watch faces

Run the python file in the same directory that contains all of the source images.  Each image is addressed individually.

Inside the script, there are arrays at the top and a few other dimensions, the rest of the file can be left alone.  

This is what to look for to change: 


#set the input file names **only takes .png
#files are all files to be tiled
#rotateMe is files that will tell time or be rotated around the axes
#stayStill are files that are statically located but need to be tiled anyways
files = ["Boat1","Boat2","FishLine","Shark1","Shark2","Water1","Water2", "Sun", "Moon"]
rotateMe = ["Shark1","Shark2", "FishLine"]
stayStill = ["Boat1","Boat2","Water1","Water2", "Sun", "Moon"]

#set the out image size in pixels
pIX, pIY = 600, 600
#set the tile size
width, height = 24, 24